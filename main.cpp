#include <SFML/Graphics.hpp>
#include <string>
#include <iostream>
#include <cstdlib>

#include "include/character/Character.h"
#include "include/character/Engineer.h"
#include "include/Map.h"
#include "include/Fps.h"
#include "include/editor/Editor.h"
#include "include/editor/Panel.h"



#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 600
#define EDITOR_ACTIVATE 1
#define EDITOR_NOT_ACTIVATE 0

sf::RenderWindow window;
sf::Event event;
sf::View view;
sf::Vector2i pixelPos;
sf::Vector2f worldPos;

Map mainMap;
Engineer mainEngineer(30,30);
Fps mainFps;
Editor mainEditor;
Panel mainPanel;
float frame;
int pressTab, pressS;


int main(){


    window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Space Survival");

    window.setFramerateLimit(60); // fps limit
    //window.setVerticalSyncEnabled(true);


    mainFps.setX(WINDOW_WIDTH-50);
    mainFps.setY(0);


    mainEditor.setActivate(0);


    while(window.isOpen()){

        /* Event */
        while(window.pollEvent(event)){

            if(event.type == sf::Event::Closed){

                window.close();
            }
        }

        /************************ View ************************/
        view.reset(sf::FloatRect(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT));
        sf::Vector2f position(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);



        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Tab) && pressTab==0){


            if(mainEditor.getActivate() == EDITOR_ACTIVATE){

                mainEditor.setActivate(EDITOR_NOT_ACTIVATE);
                mainMap.loadStructureLayer();

            }else{

                mainEditor.setActivate(EDITOR_ACTIVATE);
            }
            pressTab=1;
        }

        if(!sf::Keyboard::isKeyPressed(sf::Keyboard::Tab)){

            pressTab=0;
        }



        /************************ Editor ************************/
        if(mainEditor.getActivate() == EDITOR_ACTIVATE){



            /************************ Display ************************/
            mainEditor.display(window);
            mainPanel.display(window);




            /************************ Action ************************/
            if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){

                /* Conversion pixel map => pixel window */
                pixelPos = sf::Mouse::getPosition(window);
                worldPos = window.mapPixelToCoords(pixelPos);
                mainEditor.select(worldPos.x, worldPos.y);
            }

            if(sf::Keyboard::isKeyPressed((sf::Keyboard::S)) && pressS==0){

                mainEditor.save();
                pressS=1;
            }
            if(!sf::Keyboard::isKeyPressed((sf::Keyboard::S))){

                pressS=0;
            }

            mainEditor.setColor(mainPanel.choice());



            /************************ View ************************/
            mainEditor.updateView();

            position.x = mainEditor.getViewX();
            position.y = mainEditor.getViewY();
            view.reset(sf::FloatRect(position.x, position.y, WINDOW_WIDTH, WINDOW_HEIGHT));

            mainPanel.setX(position.x);
            mainPanel.setY(position.y);



        /************************ Not Editor ************************/
        }else if(mainEditor.getActivate() == EDITOR_NOT_ACTIVATE){



            /************************ Display ************************/
            mainMap.displayStructureLayer(window);
            mainEngineer.display(window);



            /************************ Move ************************/
            mainEngineer.progress(frame, mainMap);



            /************************ View ************************/
            position.x = mainEngineer.getX() + (TILE_SIZE / 2) - (WINDOW_WIDTH / 2);
            position.y = mainEngineer.getY() + (TILE_SIZE / 2) - (WINDOW_HEIGHT / 2);
            view.reset(sf::FloatRect(position.x, position.y, WINDOW_WIDTH, WINDOW_HEIGHT));
        }


        window.setView(view);



        /************************ Fps ************************/
        mainFps.calculate();

        mainFps.display(window);

        frame = mainFps.getFrameTime();
        if(frame > 30){

            frame = 29;
        }

        mainFps.setX(position.x+(WINDOW_WIDTH/2)); // Center
        mainFps.setY(position.y);






        window.display();
        window.clear(sf::Color(212, 224, 223));
    }
    return 0;
}

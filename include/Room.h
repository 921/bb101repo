#ifndef ROOM_H
#define ROOM_H

#include <iostream>
#include <string>

class Room{

    public:
        Room(std::string name);
        virtual ~Room();
        void setName(std::string name);
    protected:
    private:
        std::string name;
};

#endif // ROOM_H

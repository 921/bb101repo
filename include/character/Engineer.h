#ifndef ENGINEER_H
#define ENGINEER_H

#include "Character.h"



class Engineer : public Character{

    public:
        Engineer(int x, int y);
        virtual ~Engineer();
        void display(sf::RenderWindow &window);
    protected:
    private:
        sf::Texture engineer;
        sf::Sprite sprite_engineer;
};

#endif

#ifndef CHARACTER_H
#define CHARACTER_H

#include "../Map.h"

#define SPEED 0.5f

class Character{

    public:
        Character();
        virtual ~Character();
        void setX(int x);
        void setY(int y);
        int getX() const;
        int getY() const;
        void progress(float elapsed, Map laMap);
    protected:
    private:
        int x, y, widthWindow, heightWindow;
};

#endif // CHARACTER_H

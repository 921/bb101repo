#ifndef ENGINEER_H
#define ENGINEER_H

#include "../include/Character.h"



class Engineer : public Character{

    public:
        Engineer(int x, int y);
        void display(sf::RenderWindow &window);
    protected:
    private:
};

#endif

#ifndef EDITOR_H
#define EDITOR_H

#define MAP_HEIGH_EDITOR 30
#define MAP_WIDTH_EDITOR 30
#define TILE_WIDTH_EDITOR 30
#define TILE_HEIGHT_EDITOR 30
#define WALL 0
#define FLOOR 1
#define DOOR 2

class Editor{

    public:
        Editor();
        int getActivate();
        void setActivate(bool activate);
        void display(sf::RenderWindow &window);
        int dansCellule();
        void select();
        void initialize();
        void draw(int idCase);
    protected:
    private:
        int StructurLayerEditor[MAP_WIDTH_EDITOR][MAP_HEIGH_EDITOR];
        int activate;
        sf::RectangleShape caseStructureLayerEditor;
};

#endif // EDITOR_H

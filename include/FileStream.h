#ifndef FILESTREAM_H
#define FILESTREAM_H

#include <SFML/System.hpp>
#include <string>
#include <cstdio>

class FileStream : public sf::InputStream
{
public :

    FileStream();

    ~FileStream();

    bool open(const std::string& filename);

    virtual sf::Int64 read(void* data, sf::Int64 size);

    virtual sf::Int64 seek(sf::Int64 position);

    virtual sf::Int64 tell();

    virtual sf::Int64 getSize();

    virtual sf::Int64 write(int val);

    virtual sf::Int64 deleteFileContent();
private :

    std::FILE* m_file;
};

#endif // FILESTREAM_H

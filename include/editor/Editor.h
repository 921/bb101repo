#ifndef EDITOR_H
#define EDITOR_H

#include "../FileStream.h"


#define MAP_HEIGH_EDITOR 30
#define MAP_WIDTH_EDITOR 30
#define TILE_SIZE_EDITOR 30
#define WALL 0
#define FLOOR 1
#define DOOR 2
#define SPACESUIT 3

class Editor{

    public:
        Editor();
        virtual ~Editor();
        int getActivate();
        void setActivate(int activate);
        void display(sf::RenderWindow &window);
        int draw(int i, int j);
        int getCoordinateOnMap(float coordinate);
        void select(int xSouris, int ySouris);
        void setColor(int color);
        int getColor();
        void save();
        void loadStructureLayerEditor();
        void updateView();
        void setViewX(int viewX);
        int getViewX();
        void setViewY(int viewY);
        int getViewY();
    protected:
    private:
        int StructurLayerEditor[MAP_WIDTH_EDITOR][MAP_HEIGH_EDITOR];
        int RoomLayer[MAP_WIDTH_EDITOR][MAP_HEIGH_EDITOR];
        int activate, color, viewX, viewY;
        sf::RectangleShape caseStructureLayerEditor;
        FileStream fileStreamEditor;
        int lu, file, i, j, nb, change;
        char data;
        sf::Texture engineer;
        sf::Sprite sprite_engineer;
};

#endif // EDITOR_H

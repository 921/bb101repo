#ifndef PANEL_H
#define PANEL_H

#include <list>

#define TILE_WIDTH_EDITOR_PANEL 40
#define TILE_HEIGHT_EDITOR_PANEL 40
#define WALL 0
#define FLOOR 1
#define DOOR 2
#define SPACESUIT 3

class Panel{

    public:
        Panel();
        virtual ~Panel();
        void setX(int x);
        void setY(int y);
        int getX() const;
        int getY() const;
        void display(sf::RenderWindow &window);
        int choice();
        void setColor(int color);
        int getColor();
    protected:
    private:
        std::list<sf::RectangleShape> casesEditorPanel;
        sf::RectangleShape caseWall;
        sf::RectangleShape caseFloor;
        sf::RectangleShape caseDoor;
        sf::RectangleShape caseSpaceSuit;
        int x, y, color;
        sf::Texture engineer;
        sf::Sprite sprite_engineer;
};

#endif // PANEL_H

#ifndef OBJECT_H
#define OBJECT_H

#include <list>
#include "Component.h"


class Object{

    public:
        Object();
        virtual ~Object();
    protected:
    private:
        std::list<Component> components;
};



#endif // OBJECT_H

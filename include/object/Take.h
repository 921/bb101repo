#ifndef TAKE_H
#define TAKE_H

#include "Object.h"

class Take : public Object{

    public:
        Take();
        virtual ~Take();

    protected:
    private:
};

#endif // TAKE_H

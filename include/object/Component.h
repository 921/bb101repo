#ifndef COMPONENT_H
#define COMPONENT_H

#include <string>



class Component
{
    public:
        Component();
        virtual ~Component();
    protected:
    private:
        std::string name;
};

#endif // COMPONENT_H

#ifndef DONOTTAKE_H
#define DONOTTAKE_H

#include "Object.h"

class DoNotTake : public Object{

    public:
        DoNotTake();
        virtual ~DoNotTake();

    protected:
    private:
};

#endif // DONOTTAKE_H

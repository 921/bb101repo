#ifndef CHARACTER_H
#define CHARACTER_H

#include "../include/Map.h"

#define SPEED 0.5

class Character{

    public:
        sf::Texture engineer;
        sf::Sprite sprite_engineer;
        Character();
        void setX(int x);
        void setY(int y);
        int getX() const;
        int getY() const;
        void progress(float elapsed);
    protected:
    private:
        int x, y, widthWindow, heightWindow;
        Map laMap;
};

#endif // CHARACTER_H

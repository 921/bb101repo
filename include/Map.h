#ifndef MAP_H
#define MAP_H

#include <list>
#include "../include/Room.h"
#include "../include/FileStream.h"

#define MAP_WIDTH 30
#define MAP_HEIGH 30
#define TILE_SIZE 30

#define WALL 0
#define FLOOR 1
#define DOOR 2


class Map{

    public:
        Map();
        virtual ~Map();
        int getCoordinateOnMap(float coordinate);
        int collision(int xCharacter, int yCharacter);
        void displayStructureLayer(sf::RenderWindow &window);
        void loadStructureLayer();
        std::vector<sf::RectangleShape> displayStructureLayer2();
    protected:
    private:
        int StructurLayer[MAP_WIDTH][MAP_HEIGH];
        int RoomLayer[MAP_WIDTH][MAP_HEIGH];
        int ObjectLayer[MAP_WIDTH][MAP_HEIGH];
        std::list<Room> rooms;
        sf::RectangleShape caseStructureLayer;
        FileStream fileStream;
        int lu, file, i, j, nb;
        char data;
        sf::Texture engineer;
        sf::Sprite sprite_engineer;
};

#endif // MAP_H

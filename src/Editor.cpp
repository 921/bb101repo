#include <SFML/Graphics.hpp>
#include <iostream>

#include "../include/Editor.h"




Editor::Editor(){
}


int Editor::getActivate(){

    return this->activate;
}

void Editor::setActivate(bool activate){

    this->activate = activate;
}

void Editor::display(sf::RenderWindow &window){

    int x = 0, y = 0;

    for(int i=0; i<MAP_HEIGH_EDITOR; i++){

        for(int j=0; j<MAP_WIDTH_EDITOR; j++){

            caseStructureLayerEditor.setSize(sf::Vector2f(TILE_WIDTH_EDITOR, TILE_HEIGHT_EDITOR));
            caseStructureLayerEditor.setPosition(sf::Vector2f(x, y));

            if(StructurLayerEditor[i][j] == WALL){

                caseStructureLayerEditor.setFillColor(sf::Color::Black);
            }else if(StructurLayerEditor[i][j] == FLOOR){

                caseStructureLayerEditor.setFillColor(sf::Color::Blue);
            }else if(StructurLayerEditor[i][j] == DOOR){

                caseStructureLayerEditor.setFillColor(sf::Color::Cyan);
            }

            window.draw(caseStructureLayerEditor);

            x+=TILE_WIDTH_EDITOR;
        }
        x=0;
        y+=TILE_HEIGHT_EDITOR;
    }
}

int Editor::dansCellule(){

    return 1;
}

void Editor::select(){

}

void Editor::initialize(){

    for(int i=0; i<MAP_HEIGH_EDITOR; i++){

        for(int j=0; j<MAP_WIDTH_EDITOR; j++){

            StructurLayerEditor[i][j] = FLOOR;
        }
    }
}

void Editor::draw(int idCase){


}


#include <SFML/Graphics.hpp>
#include <list>

#include "../include/Map.h"


#define WALL 0
#define FLOOR 1
#define DOOR 2
#define SPACESUIT 3


Map::Map(){

    loadStructureLayer();
}

Map::~Map(){
    //dtor
}

int Map::getCoordinateOnMap(float coordinate){
    return (coordinate/TILE_SIZE);
}

int Map::collision(int x, int y){

    if(x>=0 && y>=0 && x<MAP_WIDTH && y <MAP_HEIGH){

        //if(StructurLayer[y][x]== WALL || StructurLayer[y+1][x]== WALL || StructurLayer[y][x+1]== WALL || StructurLayer[y+1][x+1]== WALL){
        if(StructurLayer[y][x] == WALL){
            std::cout << "col " << StructurLayer[0][0] << std::endl;
            return 1;
        }else return 0;
    }else return 1;
}

void Map::loadStructureLayer(){

    i=0, j=0;
    file = fileStream.open("test.txt");
    lu=fileStream.read(&data, sizeof(data));

    while(lu!=0){

        nb = data-'0';

        if(((nb == 1) || (nb == 0) || (nb == 2) || (nb == 3))){

            StructurLayer[i][j] = nb;

            j++;
            if(j==MAP_WIDTH){
                j=0;
                i++;
            }
        }

        lu=fileStream.read(&data,sizeof(data));
   }
   std::cout << StructurLayer[0][0] << std::endl;
}



void Map::displayStructureLayer(sf::RenderWindow &window){

    int x = 0, y = 0;

    for(int i=0; i<MAP_HEIGH; i++){

        for(int j=0; j<MAP_WIDTH; j++){

            caseStructureLayer.setSize(sf::Vector2f(TILE_SIZE, TILE_SIZE));
            caseStructureLayer.setPosition(sf::Vector2f(x, y));

            if(StructurLayer[i][j] == WALL){

                caseStructureLayer.setFillColor(sf::Color::Black);
            }else if(StructurLayer[i][j] == FLOOR){

                caseStructureLayer.setFillColor(sf::Color::Blue);
            }else if(StructurLayer[i][j] == DOOR){

                caseStructureLayer.setFillColor(sf::Color::Cyan);
            }else if(StructurLayer[i][j] == SPACESUIT){

                caseStructureLayer.setFillColor(sf::Color::Yellow);
            }

            window.draw(caseStructureLayer);

            if(StructurLayer[i][j] == SPACESUIT){

                caseStructureLayer.setFillColor(sf::Color::Yellow);
                if (!engineer.loadFromFile("picture/object/spaceSuit.jpg")){

                    std::cout << "Error loading spaceSuit.png" << std::endl;
                }

                sprite_engineer.setTexture(engineer);
                sprite_engineer.setPosition(sf::Vector2f(x, y));

                window.draw(sprite_engineer);
            }

            x+=TILE_SIZE;
        }
        x=0;
        y+=TILE_SIZE;
    }
}

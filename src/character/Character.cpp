#include <SFML/Graphics.hpp>
#include <iostream>

#include "../../include/character/Character.h"

Character::Character(){
}

Character::~Character(){
}

void Character::progress(float elapsed,Map laMap){

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){

        // -1 car fin dune case == x debut case suivante
        if(laMap.collision(laMap.getCoordinateOnMap(this->getX()), laMap.getCoordinateOnMap(this->getY()-SPEED*elapsed)) == 0 &&
           laMap.collision(laMap.getCoordinateOnMap(this->getX()+TILE_SIZE-1), laMap.getCoordinateOnMap(this->getY()-SPEED*elapsed)) == 0){

            setY(getY()-SPEED*elapsed);
        }else{

            setY(laMap.getCoordinateOnMap(this->getY())*TILE_SIZE);
            //setX(laMap.getCoordinateOnMap((this->getX()-(SPEED*elapsed)+1))*30+30);
        }
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){

        //if(laMap.collision(laMap.getCoordinateOnMap(this->getX()), laMap.getCoordinateOnMap(this->getY()+TILE_SIZE+SPEED*elapsed)) == 0){
        if((laMap.collision(laMap.getCoordinateOnMap(this->getX()), laMap.getCoordinateOnMap(this->getY()+TILE_SIZE+SPEED*elapsed)) == 0) &&
           (laMap.collision(laMap.getCoordinateOnMap(this->getX()+TILE_SIZE-1), laMap.getCoordinateOnMap(this->getY()+TILE_SIZE+SPEED*elapsed)) == 0)){

            setY(getY()+SPEED*elapsed);
        }else{

            setY(laMap.getCoordinateOnMap(this->getY()+TILE_SIZE/2)*TILE_SIZE);
            //setX(laMap.getCoordinateOnMap((this->getX()-(SPEED*elapsed)+1))*30+30);
        }
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){

        if((laMap.collision(laMap.getCoordinateOnMap(this->getX()-(SPEED*elapsed)), laMap.getCoordinateOnMap(this->getY())) == 0) &&
           (laMap.collision(laMap.getCoordinateOnMap(this->getX()-(SPEED*elapsed)), laMap.getCoordinateOnMap(this->getY()+TILE_SIZE-1)) == 0)){

            setX(getX()-SPEED*elapsed);
        }else{

            setX(laMap.getCoordinateOnMap(this->getX())*TILE_SIZE);
            //setX(laMap.getCoordinateOnMap((this->getX()-(SPEED*elapsed)+1))*30+30);
        }
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){

        //if(laMap.collision(laMap.getCoordinateOnMap(this->getX()+TILE_SIZE+SPEED*elapsed), laMap.getCoordinateOnMap(this->getY())) == 0){
        if((laMap.collision(laMap.getCoordinateOnMap(this->getX()+TILE_SIZE+SPEED*elapsed), laMap.getCoordinateOnMap(this->getY())) == 0) &&
           (laMap.collision(laMap.getCoordinateOnMap(this->getX()+TILE_SIZE+SPEED*elapsed), laMap.getCoordinateOnMap(this->getY()+TILE_SIZE-1)) == 0)){

            setX(getX()+SPEED*elapsed);
        }else{

            //setX(laMap.getCoordinateOnMap(this->getX()+TILE_SIZE/2)*TILE_SIZE);
            setX(laMap.getCoordinateOnMap(this->getX()+TILE_SIZE/2)*TILE_SIZE);
            //setX(laMap.getCoordinateOnMap((this->getX()-(SPEED*elapsed)+1))*30+30);
        }
    }
}


void Character::setX(int x){

    this->x = x;
}

void Character::setY(int y){

    this->y = y;
}

int Character::getX() const{

    return this->x;
}

int Character::getY() const{

    return this->y;
}

#include <SFML/Graphics.hpp>
#include <iostream>

#include "../../include/character/Engineer.h"




Engineer::Engineer(int x, int y){

    setX(x);
    setY(y);
}

Engineer::~Engineer(){
}

void Engineer::display(sf::RenderWindow &window){

    if (!engineer.loadFromFile("picture/character/engineer.jpg")){

        std::cout << "Error loading engineer.png" << std::endl;
    }

    sprite_engineer.setTexture(engineer);
    sprite_engineer.setPosition(sf::Vector2f(getX(), getY()));

    window.draw(sprite_engineer);
}

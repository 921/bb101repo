#include <SFML/Graphics.hpp>
#include <iostream>

#include "../../include/editor/Editor.h"

Editor::Editor(){

    loadStructureLayerEditor();
    this->color=0;
    this->viewX = 0;
    this->viewY = 0;
}

Editor::~Editor(){
}

int Editor::getActivate(){

    return this->activate;
}

void Editor::setActivate(int activate){

    this->activate = activate;
}

void Editor::loadStructureLayerEditor(){

    std::cout << "Loading (editor)" << std::endl;
    i=0, j=0;
    file = fileStreamEditor.open("test.txt");
    lu=fileStreamEditor.read(&data, sizeof(data));

    while(lu!=0){

        nb = data-'0';


        if(((nb == 1) || (nb == 0) || (nb == 2) || (nb == 3))){

            StructurLayerEditor[i][j] = nb;

            j++;
            if(j==MAP_WIDTH_EDITOR){
                j=0;
                i++;
            }
        }

        lu=fileStreamEditor.read(&data,sizeof(data));
   }
}

void Editor::display(sf::RenderWindow &window){

    int x = 0, y = 0;

    for(int i=0; i<MAP_HEIGH_EDITOR; i++){

        for(int j=0; j<MAP_WIDTH_EDITOR; j++){

            caseStructureLayerEditor.setSize(sf::Vector2f(TILE_SIZE_EDITOR, TILE_SIZE_EDITOR));
            caseStructureLayerEditor.setPosition(sf::Vector2f(x, y));

            if(StructurLayerEditor[i][j] == WALL){

                caseStructureLayerEditor.setFillColor(sf::Color::Black);
            }else if(StructurLayerEditor[i][j] == FLOOR){

                caseStructureLayerEditor.setFillColor(sf::Color::Blue);
            }else if(StructurLayerEditor[i][j] == DOOR){

                caseStructureLayerEditor.setFillColor(sf::Color::Cyan);
            }

            window.draw(caseStructureLayerEditor);

            if(StructurLayerEditor[i][j] == SPACESUIT){

                caseStructureLayerEditor.setFillColor(sf::Color::Yellow);
                if (!engineer.loadFromFile("picture/object/spaceSuit.jpg")){

                    std::cout << "Error loading spaceSuit.png" << std::endl;
                }

                sprite_engineer.setTexture(engineer);
                sprite_engineer.setPosition(sf::Vector2f(x, y));

                window.draw(sprite_engineer);
            }
            x+=TILE_SIZE_EDITOR+1;
        }
        x=0;
        y+=TILE_SIZE_EDITOR+1;
    }
}

int Editor::getCoordinateOnMap(float coordinate){
    return (coordinate/TILE_SIZE_EDITOR);
}

int Editor::draw(int i, int j){

    if(i>=0 && j>=0 && i<MAP_WIDTH_EDITOR && j<MAP_HEIGH_EDITOR){

        StructurLayerEditor[j][i] = getColor();
        return 1;
    }
    return 0;
}

void Editor::select(int xSouris, int ySouris){

    if(draw(getCoordinateOnMap(xSouris), getCoordinateOnMap(ySouris)) != 1){

        std::cout << "Drawing Error" << std::endl;
    }
}



void Editor::save(){

    fileStreamEditor.deleteFileContent();

    for(int i=0; i<MAP_HEIGH_EDITOR; i++){

        for(int j=0; j<MAP_WIDTH_EDITOR; j++){

            fileStreamEditor.write(StructurLayerEditor[i][j]);
        }
    }
}

void Editor::updateView(){

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z)){

        change = getViewY();
        change-=5;
        setViewY(change);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::X)){

        change = getViewY();
        change+=5;
        setViewY(change);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q)){

        change = getViewX();
        change-=5;
        setViewX(change);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::D)){

        change = getViewX();
        change+=5;
        setViewX(change);
    }
}

void Editor::setColor(int color){
    this->color = color;
}

int Editor::getColor(){
    return this->color;
}

void Editor::setViewX(int viewX){
    this->viewX = viewX;
}

int Editor::getViewX(){
    return this->viewX;
}

void Editor::setViewY(int viewY){
    this->viewY = viewY;
}

int Editor::getViewY(){
    return this->viewY;
}

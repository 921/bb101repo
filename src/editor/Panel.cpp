#include <SFML/Graphics.hpp>
#include <iostream>

#include "../../include/editor/Panel.h"




Panel::Panel(){

    x = 0, y = 0;
    color=0;
}

Panel::~Panel(){
}

void Panel::display(sf::RenderWindow &window){

    /* Wall */
    caseWall.setSize(sf::Vector2f(TILE_WIDTH_EDITOR_PANEL, TILE_HEIGHT_EDITOR_PANEL));

    caseWall.setPosition(getX()+10, getY());

    caseWall.setFillColor(sf::Color::Black);

    if(getColor() == WALL){

        caseWall.setOutlineColor(sf::Color::Green);
        caseWall.setOutlineThickness(5);
    }else{

        caseWall.setOutlineThickness(0);
    }

    window.draw(caseWall);


    /* Floor */
    caseFloor.setSize(sf::Vector2f(TILE_WIDTH_EDITOR_PANEL, TILE_HEIGHT_EDITOR_PANEL));

    caseFloor.setPosition(getX()+TILE_WIDTH_EDITOR_PANEL+TILE_WIDTH_EDITOR_PANEL/2, getY());

    caseFloor.setFillColor(sf::Color::Blue);

    if(getColor() == FLOOR){

        caseFloor.setOutlineColor(sf::Color::Green);
        caseFloor.setOutlineThickness(5);
    }else{

        caseFloor.setOutlineThickness(0);
    }

    window.draw(caseFloor);


    /* Door */
    caseDoor.setSize(sf::Vector2f(TILE_WIDTH_EDITOR_PANEL, TILE_HEIGHT_EDITOR_PANEL));

    caseDoor.setPosition(getX()+TILE_WIDTH_EDITOR_PANEL*3, getY());

    caseDoor.setFillColor(sf::Color::Cyan);

    if(getColor() == DOOR){

        caseDoor.setOutlineColor(sf::Color::Green);
        caseDoor.setOutlineThickness(5);
    }else{

        caseDoor.setOutlineThickness(0);
    }

    window.draw(caseDoor);


    /* SpaceSuit */
    caseSpaceSuit.setSize(sf::Vector2f(TILE_WIDTH_EDITOR_PANEL, TILE_HEIGHT_EDITOR_PANEL));

    caseSpaceSuit.setPosition(getX()+TILE_WIDTH_EDITOR_PANEL*4+TILE_WIDTH_EDITOR_PANEL/2, getY());

    caseSpaceSuit.setFillColor(sf::Color::Yellow);

    if(getColor() == SPACESUIT){

        caseSpaceSuit.setOutlineColor(sf::Color::Green);
        caseSpaceSuit.setOutlineThickness(5);
    }else{

        caseSpaceSuit.setOutlineThickness(0);
    }

    window.draw(caseSpaceSuit);

    if (!engineer.loadFromFile("picture/object/spaceSuit.jpg")){

        std::cout << "Error loading spaceSuit.png" << std::endl;
    }

    sprite_engineer.setTexture(engineer, false);

    sprite_engineer.setPosition(sf::Vector2f(caseSpaceSuit.getPosition().x, caseSpaceSuit.getPosition().y));
    sprite_engineer.setScale(sf::Vector2f(1.3f, 1.2f));
    window.draw(sprite_engineer);



}

int Panel::choice(){

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad0)){

        setColor(WALL);
    }else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad1)){

        setColor(FLOOR);
    }else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad2)){

        setColor(DOOR);
    }else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad3)){

        setColor(SPACESUIT);
    }

    return getColor();
}

void Panel::setColor(int color){
    this->color = color;
}

int Panel::getColor(){
    return this->color;
}

void Panel::setX(int x){

    this->x = x;
}

void Panel::setY(int y){

    this->y = y;
}

int Panel::getX() const{

    return this->x;
}

int Panel::getY() const{

    return this->y;
}

#include <SFML/Graphics.hpp>
#include <iostream>

#include "../include/Engineer.h"




Engineer::Engineer(int x, int y){

    setX(x);
    setY(y);
}

void Engineer::display(sf::RenderWindow &window){

    if (!engineer.loadFromFile("picture/engineer.jpg")){

        std::cout << "Error loading engineer.png" << std::endl;
    }

    sprite_engineer.setTexture(engineer);
    sprite_engineer.setPosition(sf::Vector2f(getX(), getY()));

    window.draw(sprite_engineer);
}

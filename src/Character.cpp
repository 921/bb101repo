#include <SFML/Graphics.hpp>
#include <iostream>

#include "../include/Character.h"

Character::Character(){
}

void Character::progress(float elapsed){

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){

        if(laMap.getCharacterLocationOnMap(sprite_engineer.getPosition().x, sprite_engineer.getPosition().y-SPEED*elapsed) == 0){
            //std::cout << elapsed << std::endl;
            sprite_engineer.move(0, -SPEED*elapsed);
        }

    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){

        if(laMap.getCharacterLocationOnMap(sprite_engineer.getPosition().x, sprite_engineer.getPosition().y+SPEED*elapsed) == 0){

            sprite_engineer.move(0, SPEED*elapsed);
        }
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){

        if(laMap.getCharacterLocationOnMap(sprite_engineer.getPosition().x-SPEED*elapsed, sprite_engineer.getPosition().y) == 0){

            sprite_engineer.move(-SPEED*elapsed, 0);
        }
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){

        if(laMap.getCharacterLocationOnMap(sprite_engineer.getPosition().x+SPEED*elapsed, sprite_engineer.getPosition().y) == 0){

            sprite_engineer.move(SPEED*elapsed, 0);
        }
    }

    setX(sprite_engineer.getPosition().x);
    setY(sprite_engineer.getPosition().y);


}


void Character::setX(int x){

    this->x = x;
}

void Character::setY(int y){

    this->y = y;
}

int Character::getX() const{

    return this->x;
}

int Character::getY() const{

    return this->y;
}

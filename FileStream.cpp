#include <SFML/Graphics.hpp>

#include "include/FileStream.h"
#include <iostream>

FileStream::FileStream() :m_file(NULL){
}

FileStream::~FileStream(){

    if (m_file)
        std::fclose(m_file);
}

bool FileStream::open(const std::string& filename){

    if (m_file)
        std::fclose(m_file);

    m_file = std::fopen(filename.c_str(), "r+");

    return m_file != NULL;
}

sf::Int64 FileStream::read(void* data, sf::Int64 size){

    if (m_file)
        return std::fread(data, 1, static_cast<std::size_t>(size), m_file);
    else
        return -1;
}

sf::Int64 FileStream::seek(sf::Int64 position){

    if (m_file)
    {
        std::fseek(m_file, static_cast<std::size_t>(position), SEEK_SET);
        return tell();
    }
    else
    {
        return -1;
    }
}

sf::Int64 FileStream::tell(){

    if (m_file)
        return std::ftell(m_file);
    else
        return -1;
}

sf::Int64 FileStream::getSize(){

    if (m_file)
    {
        sf::Int64 position = tell();
        std::fseek(m_file, 0, SEEK_END);
        sf::Int64 size = tell();
        seek(position);
        return size;
    }
    else
    {
        return -1;
    }
}

sf::Int64 FileStream::deleteFileContent(){

    if (m_file)
        std::fclose(m_file);

    m_file = std::fopen("test.txt", "w+t");
    std::fclose(m_file);
}

sf::Int64 FileStream::write(int val){




    if (m_file)
        std::fclose(m_file);

    m_file = std::fopen("test.txt", "a+t");


    fprintf(m_file, "%d",val);
    //std::fclose(m_file);

    return 1;
}

